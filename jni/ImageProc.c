#include "ImageProc.h"
int errnoexit(const char *s)
{
	return ERROR_LOCAL;
}
int xioctl(int fd, int request, void *arg)
{
	int r;
	do r = ioctl (fd, request, arg);
	while (-1 == r && EINTR == errno);
	return r;
}
int Open(int i)
{
	struct stat st;
	char system_call_buffer[100];
	sprintf(dev_name,"/dev/video%d",i);
	sprintf(system_call_buffer,"su -c \"chmod 666 %s\"",dev_name);
	int ret = system(system_call_buffer);
	if (ret !=0) {
		return ERROR_LOCAL;
	}
	if (-1 == stat (dev_name, &st)) {
		return ERROR_LOCAL;
	}
	if (!S_ISCHR (st.st_mode)) {
		return ERROR_LOCAL;
	}
	int fd = open (dev_name, O_RDWR | O_NONBLOCK, 0);
	if (-1 == fd) {
		return ERROR_LOCAL;
	}
	return fd;
}
int InitializeDevice(int fd) 
{
	struct v4l2_capability cap;
	struct v4l2_cropcap cropcap;
	struct v4l2_crop crop;
	struct v4l2_format fmt;
	unsigned int min;
	if (-1 == xioctl (fd, VIDIOC_QUERYCAP, &cap)) {
		if (EINVAL == errno) {
			return ERROR_LOCAL;
		} else {
			return errnoexit ("VIDIOC_QUERYCAP");
		}
	}
	if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
		return ERROR_LOCAL;
	}
	if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
		return ERROR_LOCAL;
	}
	CLEAR (cropcap);
	cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (0 == xioctl (fd, VIDIOC_CROPCAP, &cropcap)) {
		crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		crop.c = cropcap.defrect; 
		if (-1 == xioctl (fd, VIDIOC_S_CROP, &crop)) {
			switch (errno) {
				case EINVAL:
					break;
				default:
					break;
			}
		}
	}
	CLEAR (fmt);
	fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width       = IMG_WIDTH; 
	fmt.fmt.pix.height      = IMG_HEIGHT;
	fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
	fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;
	if (-1 == xioctl (fd, VIDIOC_S_FMT, &fmt))
		return errnoexit ("VIDIOC_S_FMT");
	min = fmt.fmt.pix.width * 2;
	if (fmt.fmt.pix.bytesperline < min)
		fmt.fmt.pix.bytesperline = min;
	min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
	if (fmt.fmt.pix.sizeimage < min)
		fmt.fmt.pix.sizeimage = min;
	return SUCCESS_LOCAL;
}
int GetNumberOfBuffersToInitialize(int fd)
{
	struct v4l2_requestbuffers req;
	CLEAR (req);
	req.count               = 4;
	req.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory              = V4L2_MEMORY_MMAP;
	if (-1 == xioctl (fd, VIDIOC_REQBUFS, &req)) {
		if (EINVAL == errno) {
			return ERROR_LOCAL;
		} else {
			return errnoexit ("VIDIOC_REQBUFS");
		}
	}
	if (req.count < 2) {
		return ERROR_LOCAL;
 	}
	return req.count;
}
void* SetupBuffer(int fd, int index, int *length)
{
	struct v4l2_buffer buf;
	CLEAR (buf);
	buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory      = V4L2_MEMORY_MMAP;
	buf.index       = index;
	if (-1 == xioctl (fd, VIDIOC_QUERYBUF, &buf))
		return errnoexit ("VIDIOC_QUERYBUF");
	*length=buf.length;
	return mmap (NULL ,
			buf.length,
			PROT_READ | PROT_WRITE,
			MAP_SHARED,
			fd, buf.m.offset);
}
int StartCapturing(int fd,int NumBuffers)
{
	unsigned int i;
	enum v4l2_buf_type type;
	for (i = 0; i < NumBuffers; ++i) {
		struct v4l2_buffer buf;
		CLEAR (buf);
		buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory      = V4L2_MEMORY_MMAP;
		buf.index       = i;
		if (-1 == xioctl (fd, VIDIOC_QBUF, &buf))
			return errnoexit ("VIDIOC_QBUF");
	}
	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == xioctl (fd, VIDIOC_STREAMON, &type))
		return errnoexit ("VIDIOC_STREAMON");
	return SUCCESS_LOCAL;
}
int StopCapturing(int fd)
{
	enum v4l2_buf_type type;
	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == xioctl (fd, VIDIOC_STREAMOFF, &type))
		return errnoexit ("VIDIOC_STREAMOFF");
	return SUCCESS_LOCAL;
}
int RequestFrame(int fd, int *BuffIndex)
{
	struct v4l2_buffer buf;
	CLEAR (buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	if (-1 == xioctl (fd, VIDIOC_DQBUF, &buf)) {
		switch (errno) {
			case EAGAIN:
				return -1;
			case EIO:
			default:
				return errnoexit ("VIDIOC_DQBUF");
		}
	}
	*BuffIndex=buf.index;
	return 0;
}
int DisposeFrame(int fd)
{
	struct v4l2_buffer buf;
	CLEAR (buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	if (-1 == xioctl (fd, VIDIOC_QBUF, &buf))
		return errnoexit ("VIDIOC_QBUF");
	return 0;
}
int UninitializeBuffer(void* start,int length)
{
	if (-1 == munmap (start, length))
		return errnoexit ("munmap");
	return SUCCESS_LOCAL;
}
int Select(int fd)
{
	fd_set fds;
	struct timeval tv;
	int r;
	FD_ZERO (&fds);
	FD_SET (fd, &fds);
	tv.tv_sec = 2;
	tv.tv_usec = 0;
	r = select (fd + 1, &fds, NULL, NULL, &tv);
	if (-1 == r) 
	{
		if (EINTR != errno)
		return errnoexit ("select");
	}
	if (0 == r) 
		return ERROR_LOCAL;
	return 0;
}
int Close(int fd)
{
	close (fd);
	fd = -1;
	return SUCCESS_LOCAL;
}