#include <jni.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <asm/types.h>
#include <linux/videodev2.h>
#include <linux/usbdevice_fs.h>
#define CLEAR(x) memset (&(x), 0, sizeof (x))
#define IMG_WIDTH 640
#define IMG_HEIGHT 480
#define ERROR_LOCAL -1
#define SUCCESS_LOCAL 0
static char dev_name[16];
int errnoexit(const char *s);
int xioctl(int fd, int request, void *arg);
int Select(int fd);
int UninitializeBuffer(void* start,int length);
int DisposeFrame(int fd);
int RequestFrame(int fd,int *BuffIndex);
int StopCapturing(int fd);
int StartCapturing(int fd,int NumBuffers);
void* SetupBuffer(int fd,int index,int *length);
int GetNumberOfBuffersToInitialize(int fd);
int InitializeDevice(int fd);
int Open(int i);
int Close(int fd);